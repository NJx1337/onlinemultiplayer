// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuSystem/MainMenu.h"
#include "MenuSystem/ServerRow.h"
#include "Components/Button.h"

void UServerRow::Setup(class UMainMenu* Parent, uint32 Index)
{
	this->Parent = Parent;
	this->Index = Index;
}

bool UServerRow::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	if (!ensure(RowButton != nullptr)) return false;
	RowButton->OnClicked.AddDynamic(this, &UServerRow::OnClicked);

	return true;
}

void UServerRow::OnClicked()
{
	Parent->SelectIndex(Index);
}
